<?php
/**
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 09.01.2017
 * Time: 17:56
 */

namespace RussianRobotics;


class CounterDownload
{
	protected $path;
	protected $file;
	protected $pathCounter;
	protected $name;
	protected $hashName;
	protected $sKey;

	protected static $mode = 0755;

	function __construct($file, $path = '/data/downloads')
	{
		session_start();
		$this->sessionKey();

		$this->path = rtrim(trim($path), DIRECTORY_SEPARATOR);
		$this->file = DIRECTORY_SEPARATOR . ltrim(trim($file), DIRECTORY_SEPARATOR);
		$this->init();
	}

	function __destruct()
	{
		session_write_close();
	}

	protected function init()
	{
		$pathFile = $_SERVER['DOCUMENT_ROOT'] . $this->file;
		$this->name = md5($this->file) . ".dat";

		if(!file_exists($pathFile))
		{
			throw new \Exception("Файл $this->file не найден");
		}

		$fullPath = $this::createFolders($this->path);

		$this->pathCounter = $fullPath . DIRECTORY_SEPARATOR . $this->name;
		if(!file_exists($this->pathCounter)) {
			$this->setCounter(0);
		}
	}

	protected static function createFolders($path)
	{
		$folders = explode(DIRECTORY_SEPARATOR, $path);
		$currentFolder = $_SERVER['DOCUMENT_ROOT'];

		foreach($folders as $folder)
		{
			if(!empty($folder))
			{
				$currentFolder .= DIRECTORY_SEPARATOR . $folder;
				if(!file_exists($currentFolder) || !is_dir($currentFolder)) {
					$result = mkdir($currentFolder, self::$mode);
					if($result === false) {
						throw new \Exception("Невозможно создать директорию $path");
					}
				}
			}
		}

		return $currentFolder;
	}

	/**
	 * Возвращает кол-во скачиваний файла
	 * @return int
	 * @throws \Exception
	 */
	public function getCounter()
	{
		$data = file_get_contents($this->pathCounter);

		if($data === false) {
			throw new \Exception("Невозможно прочитать файл $this->name");
		}

		$data = intval($data);
		return $data;
	}

	protected function sessionKey()
	{
		$this->sKey = md5(__CLASS__);
	}

	protected function setCounter($count)
	{
		$count = intval($count);

		$result = file_put_contents($this->pathCounter, "$count", LOCK_EX);
		if($result === false) {
			throw new \Exception("Невозможно записать файл $this->name");
		}
	}

	protected function isMarked()
	{
		return (isset($_SESSION[$this->sKey][$this->name]) && $_SESSION[$this->sKey][$this->name]=='y');
	}

	protected function mark()
	{
		$_SESSION[$this->sKey][$this->name] = 'y';
	}

	/**
	 * Увеличивает счетчик кол-во скачиваний
	 * @return int
	 * @throws \Exception
	 */
	protected function increment()
	{
		$count = $this->getCounter();

		if(!$this->isMarked())
		{
			$count += 1;
			$this->setCounter($count);
			$this->mark();
		}

		return $count;
	}

	/**
	 * Отдать файл на скачивание
	 */
	public function sendToDownload()
	{
		$pathFile = $_SERVER['DOCUMENT_ROOT'] . $this->file;

		if (file_exists($pathFile)) {

			//Увеличиваем счетчик скачиваний
			$this->increment();

			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}

			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: ' . mime_content_type($pathFile) );
			header('Content-Disposition: attachment; filename=' . basename($pathFile));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($pathFile));

			// читаем файл и отправляем его пользователю
			readfile($pathFile);
			exit;
		}
	}
}